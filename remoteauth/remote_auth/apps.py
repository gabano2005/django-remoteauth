from django.apps import AppConfig


class AuthConfig(AppConfig):
    name = 'remote_auth'
